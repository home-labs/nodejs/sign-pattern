import { Clonable } from "../src/lib/index.js";


const s1 = Symbol('foo');

class C1 implements Clonable {

    p1 = 1;

    p2 = 2;

    // não aceita, o que é o certo
    // getClone(): InstanceState<C1> {
    getClone(): this {
        return Object.create(this);
    }

    m1(): number {
        return this[s1]();
    }

    private [s1](): number {
        return this.p1;
    }

}

const c1 = new C1;

const c1Clone = c1.getClone();

c1Clone.p1 = 2;

console.log(`original value of p1: `, c1.m1())
console.log(`modified value of p1 (by a clone): `, c1Clone.p1)
console.log(`value of p1 (has not been changed): `, c1.m1())
