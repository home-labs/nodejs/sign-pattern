import { InstanceState } from "@cyberjs.on/types";

import { Memento } from "./memento";
import { Originator } from "./originator";


export class Storage<T> {

    private history: Memento<InstanceState<T>>[];

    constructor(
        private originator: Originator<T>
    ) {

        this.history = [];

    }

    storeCurrentState(): void {
        this.history.unshift(this.originator.createMemento());
    }

    recoverPreviousState(): void {
        if (this.history.length >= 1) {
            this.originator.restoreState(this.history.shift() as any);
        }
    }

}
