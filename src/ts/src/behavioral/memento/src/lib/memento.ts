import { InstanceState } from '@cyberjs.on/types';


export interface Memento<T> {

    state: InstanceState<T>;

}
