import {
    Memento,
    Originator,
    Storage
} from '../index.js';
import { InstanceState } from '@cyberjs.on/types';


const s1 = Symbol('foo');

class C1 {

    p1 = 1;

    p2 = 2;

    m1(): number {
        return this[s1]();
    }

    private [s1](): number {
        return this.p1;
    }

}


export class C1Memento implements Memento<C1> {

    get state(): InstanceState<C1> {
        return this.c1State;
    }

    constructor(
        private c1State: InstanceState<C1>
    ) {

    }

}

// uses Memento
export class C1Originator implements Originator<C1> {

    constructor(
        private c1: C1
    ) {

    }

    createMemento(): Memento<InstanceState<C1>> {
        return new C1Memento(Object.assign({}, this.c1));
    }

    restoreState(memento: Memento<C1>): void {

        // memento.state.
        Object.assign(this.c1, memento.state);

    }

}

// the Program. Uses Storage and Originator
class Test {

    private storage: Storage<C1>;

    constructor(c1Originator: Originator<C1>) {
        this.storage = new Storage(c1Originator);
    }

    recordState(): void {
        this.storage.storeCurrentState();
    }

    restoreState(): void {
        this.storage.recoverPreviousState();
    }

}

const c1 = new C1;

const c1Originator = new C1Originator(c1);

const test = new Test(c1Originator);

test.recordState();

console.log(`original value (recorded): `, c1.p1);

c1.p1 = 2;

console.log(`new value: `, c1.p1);

test.restoreState();

console.log(`recovered value: `, c1.p1);
