import {
    AbstractNotifier,
    Subscriber,
    Subscription
} from '../index.js';


// Concrete Subject
class NotifierOfString extends AbstractNotifier {

    private static instance: NotifierOfString;
    /**
     * o certo seria usar esta linha, tendo em vista que é uma forma nativa de privatizar uma propriedade no JS, de forma que tal propriedade não poderá ser nativamente acessada, mas, na hora de compilar com "npm run", o "tsc" reclamou.
     */
    // static #instance: NotifierOfString;

    private constructor() {
        super();
    }

    static getInstance(): AbstractNotifier {

        // if (this.#instance) {
        if (this.instance) {
            // return this.#instance;
            return this.instance;
        }

        // return this.#instance = new NotifierOfString();
        return this.instance = new NotifierOfString();
    }

    notify(aStringArg: string): void {
        for (let subscribe of this.subscribers) {
            subscribe.receiveNotification(aStringArg);
        }
    }

}

// Concrete Observer
class S1 implements Subscriber {

    #notifier: AbstractNotifier;

    constructor(notifier: AbstractNotifier) {
        this.#notifier = notifier;
    }

    receiveNotification(aStringArg: string): void {
        console.log(`${this.constructor.name} was notified:`, aStringArg);
    }

    getNotifier(): AbstractNotifier {
        return this.#notifier;
    }

}

class S2 implements Subscriber {

    constructor(
        private notifier: AbstractNotifier
    ) {

    }

    receiveNotification(...args: any[]): void {
        console.log(`${this.constructor.name} was notified:`, ...args);
    }

    getNotifier(): AbstractNotifier {
        return this.notifier;
    }

}

class NotifierTest {

    private notifier: NotifierOfString;

    private sb1: Subscriber;

    private sb2: Subscriber;

    private sp1: Subscription;

    private sp2: Subscription;

    constructor(
        // testar Singleton
        // não funcionou. A linguagem reclamou, ela espera um argumento. Sem contar que o Singleton foi implementado explicitamente
        // private notifier: NotifierOfString
    ) {
        this.notifier = NotifierOfString.getInstance();

        this.sb1 = new S1(this.notifier);

        this.sb2 = new S2(this.notifier);

        this.sp1 = this.notifier.addSubscriber(this.sb1);

        this.sp2 = this.notifier.addSubscriber(this.sb2);

    }

    notify() {
        this.notifier.notify(`a string arg.`);
    }

    usubscribe2() {
        this.sp2.unsubscribe();
        // this.notifier.removeSubscriber(this.sb2);
    }

    usubscribe1() {
        this.sp1.unsubscribe();
    }

}

const notifierTest = new NotifierTest();
notifierTest.notify();
notifierTest.usubscribe2();
notifierTest.notify();
