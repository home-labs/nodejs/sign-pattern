import { Subscriber } from "./subscriber.js";


export class Subscription {

    constructor(
        private subscriber: Subscriber
    ) { }

    unsubscribe() {
        // já que o inscrito sabe onde se inscreveu para ser notificado, é insteressante acrescentar um objeto para facilicar a sintaxe de remoção de inscrição, pois encapsula o inscrito
        this.subscriber.getNotifier().removeSubscriber(this.subscriber);
    }

}
