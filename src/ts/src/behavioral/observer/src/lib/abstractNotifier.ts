import { Subscriber } from "./subscriber.js";
import { Subscription } from "./subscription.js";


// Abstract Subject
export abstract class AbstractNotifier {

    protected subscribers: Subscriber[];

    constructor() {
        this.subscribers = [];
    }

    addSubscriber(subscriber: Subscriber): Subscription {

        this.subscribers.push(subscriber);

        return new Subscription(subscriber);
    }

    removeSubscriber(subscriber: Subscriber) {

        const index = this.subscribers.findIndex(
            (item: Subscriber): boolean => {
                if (item === subscriber) {
                    return true;
                }

                return false;
            }
        );

        if (index > -1) {
            this.subscribers.splice(index, 1);
        }
    }

    abstract notify(...args: any[]): void;

}
