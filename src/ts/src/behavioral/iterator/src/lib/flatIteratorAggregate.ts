import { Iterable } from "./iterable";
import { IteratorAggregate } from "./iteratorAggregate";
import { FlatIterator } from "./flatIterator.js";


// Concrete Iterator Aggregate
export class FlatIteratorAggregate<V> implements IteratorAggregate<V> {

    constructor(
        private collection: V[]
    ) {

    }

    getIterator(): Iterable<V> {
        return new FlatIterator(this.collection);
    }

}
