import { FlatIteratorAggregate } from "../index.js";
import { NumberIteratorAggregate } from "./numberIteratorAggregate.js";


const iterator: FlatIteratorAggregate<number> = new NumberIteratorAggregate([1, 2, 3, 4]);
for (let number of iterator.getIterator()) {
    console.log(number);
}
