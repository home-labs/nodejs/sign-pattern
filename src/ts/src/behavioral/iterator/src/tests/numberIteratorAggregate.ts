import { FlatIteratorAggregate } from "../index.js";


export class NumberIteratorAggregate extends FlatIteratorAggregate<number> {

    constructor(collection: number[]) {
        super(collection);
    }

}
