import { GenericAbstractFlyweight } from "./generic-abstract-flyweight";


export class GenericFlyweightWarehouse<T> {

    private flyweights!: Map<any, GenericAbstractFlyweight<T>>;

    constructor() {
        this.flyweights = new Map();
    }

    add(key: any, reference: GenericAbstractFlyweight<T>) {
        if (!this.flyweights.has(key)) {
            this.flyweights.set(key, reference);
        }
    }

    // get(key: any): T | undefined {

    //     const flyweight: GenericAbstractFlyweight<T> | undefined = this.flyweights.get(key);

    //     if (flyweight) return flyweight.getConcrete();
    // }

}
