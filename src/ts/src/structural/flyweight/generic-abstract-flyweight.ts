export interface GenericAbstractFlyweight<T> {

    getConcrete(): T;

}
